var express = require('express')

var multer = require('multer')
var fs = require('fs')
var path = require('path')

module.exports = function usersRouteBuilder (user, passport) {
  var router = express.Router()
  var upload = multer({ dest: 'uploads/' }) // temporal

  router.post('/uploadImage', upload.single('avatar'), function (req, res, next) {
    console.log(req.file)
    var nombre = req.file.originalname
    fs.rename(req.file.path, path.join('uploads', 'avatar', nombre), () => {
      res.redirect('/?show=' + nombre)
      res.end()
    })
  })
  
  router.get('/uploadImage', function (req, res, next) {
    res.render('uploadFile')
  })

  router.get('/Lista', function (req, res, next) {
    user.findAll({ limit: 10 }).then(function (retorno) {
      console.log(retorno)
    })
    res.render('uploadFile')
  })

  router.get('/logout', function (req, res, next) {
    req.session = null
    res.redirect('/users')
  })
  
  /* GET users listing. */
  router.get('/', function (req, res, next) {
    if (req.session.logeado === 1) {
      res.redirect('/')
    } else {
      res.render('login')
    }
  })

  router.post('/', passport.authenticate('local', { failureRedirect: '/error' }), function (req, res, next) {
    res.redirect('/')
    res.end()
  })
  return router
}
