var express = require('express')
var path = require('path')
var logger = require('morgan')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var cookieSession = require('cookie-session')

var index = require('./routes/index')
var users = require('./routes/users')
var api = require('./routes/api')
var app = express()

var redisDriver = require('ioredis')
var redis = redisDriver({
  host: 'localhost',
  password: 'vagrant'
})

redis.on('error', function (err) {
  console.log('Redis error: ' + err)
})
redis.on('connect', function () {
  console.log('Connected to Redis')
})

// database
const Sequelize = require('sequelize')
const sequelize = new Sequelize('toctoc', 'qadesarrollo', 'qadesarroll0', {
  host: '192.168.100.1',
  dialect: 'mssql',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
})

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err)
  })

var UserModel = require('./models/usuario.js')
const User = UserModel(sequelize)

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use('/assets/avatar', express.static(path.join(__dirname, 'uploads/avatar')))
app.use(cookieSession({
  name: 'session',
  secret: 'asdf',

  // Cookie Options
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}))

var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy

passport.use(new LocalStrategy({
  usernameField: 'usuario',
  passwordField: 'password',
  session: false
},
  function (username, password, done) {
    console.log('strategy: local', {username, password})
    User.findOne({ where: {Rut: username} })
      .then(user => {
        console.log('no errors')
        if (!user) {
          console.log('!user')
          return done(null, false)
        }
        console.log('user ok', user)
        if (!user.verifyPassword(password)) {
          console.log('password fail')
          return done(null, false)
        }
        console.log('humm... password ok', user)
        return done(null, user)
      })
      .catch(err => {
        console.log(err)
        return done(err)
      })
  }
))

app.use('/', index)
app.use('/users', users(User, passport))
app.use('/api', api)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})
// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
