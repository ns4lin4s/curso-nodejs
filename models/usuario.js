const Sequelize = require('sequelize')

module.exports = function mapUsuario (sequelize) {
  const User = sequelize.define('Usuario', {
    IdUsuario: { type: Sequelize.INTEGER, primaryKey: true },
    Nombres: {
      type: Sequelize.STRING
    },
    Rut: {
      type: Sequelize.STRING
    }
  }, {
    tableName: 'Usuario',
    freezeTableName: true,
    timestamps: false

  })

  User.prototype.verifyPassword = function (password) {
    console.log(password)
    var pass = this.Nombres
    return password === pass
  }

  return User
}
